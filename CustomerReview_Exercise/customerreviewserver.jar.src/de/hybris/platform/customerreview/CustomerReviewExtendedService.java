package de.hybris.platform.customerreview;
import de.hybris.platform.customerreview.model.CustomerReviewModel;
import de.hybris.platform.core.model.product.ProductModel;

public abstract interface CustomerReviewExtendedService extends CustomerReviewService
{
/* Create a customer review that doesn'r contain restricted words and doesn't have negative rating */
	public abstract CustomerReviewModel createCensoredCustomerReview(Double paramDouble, String paramString1, String paramString2, UserModel paramUserModel, ProductModel paramProductModel);
/*Get number of reviews for a product that have the minimum ratings min and maximum ratings max */
	public abstract Integer getNumberOfReviewsInRange(ProductModel paramProductModel, Double min, Double max);
}