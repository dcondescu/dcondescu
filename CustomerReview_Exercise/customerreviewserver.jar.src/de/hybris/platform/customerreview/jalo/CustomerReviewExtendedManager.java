package de.hybris.platform.customerreview.jalo;
import java.util.Scanner;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Iterator;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.user.User;
import java.util.Collection;
import de.hybris.platform.jalo.flexiblesearch.FlexibleSearch;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.JaloBusinessException;

public class CustomerReviewExtendedManager extends CustomerReviewManager
{
/* Read words from the file named "forbidden.txt" situated in the current directory and if any word is found in comments throw an exception.
Check rating and throw an exception if it is negative
If all the rules are passed, go ahead and create the customer review */

public CustomerReview createCensoredCustomerReview(Double rating, String headline, String comment, User user, Product product)
  {
	boolean usedForbiddenWord=false;
	String forbiddenWord=null;
	if (rating < 0) throw new JaloBusinessException(e, "error creating CustomerReview : rating " + rating + " is not allowed" , 0);
	try
	{
	Scanner s = new Scanner(new File("forbiden.txt")).useDelimiter(System.lineSeparator());
	while (s.hasNext())
		{  
		String forbiddenWord = s.next(); 		
		if (comment.toLowerCase().contains(forbiddenWord)) 
			{
			usedForbiddenWord=true;
			break;		
			}
		}
	s.close();
	}
	catch (Exception e)
	{
		throw new JaloSystemException(e, "error creating CustomerReview : " + e.getMessage(), 0);
	}
	if (usedForbiddenWord) {
				throw new JaloBusinessException(e, "error creating CustomerReview : the word " + forbiddenWord + " is not allowed in comments" , 0);
				}
	createCustomerReview(rating, headline, comment, user, product);
  }


/*Get a productís total number of customer reviews whose ratings are within a given range (inclusive)*/

public Integer getNumberOfReviewsInRange(SessionContext ctx, Product item,Double min,Double max)
  {
     String query = "SELECT count(*) FROM {" + GeneratedCustomerReviewConstants.TC.CUSTOMERREVIEW + "} WHERE {" + 
       "product" + "} = ?product" + "and {rating} >= ?" + min + " and {rating} <= ?" + max;
     
     Map<String, Object> values = new HashMap();
     values.put("product", item);
     values.put("min",min);
     values.put("max",max);
     List<Integer> result = FlexibleSearch.getInstance()
       .search(query, values, Collections.singletonList(Integer.class), true, 
       true, 
       0, -1).getResult();
     return (Integer)result.iterator().next();
   }

}