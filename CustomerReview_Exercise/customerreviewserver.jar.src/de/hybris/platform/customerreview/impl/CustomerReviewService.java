package de.hybris.platform.customerreview.impl;
import de.hybris.platform.customerreview.CustomerReviewExtendedService;
import de.hybris.platform.customerreview.jalo.CustomerReviewExtendedManager;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.customerreview.model.CustomerReviewModel;

public class CustomerReviewService extends DefaultCustomerReviewService implements CustomerReviewExtendedService
{
public CustomerReviewModel createCensoredCustomerReview(Double rating, String headline, String comment, UserModel user, ProductModel product)
  {
	CustomerReview review = CustomerReviewExtendedManager.getInstance().createCensoredCustomerReview(rating, headline, comment, (User)getModelService().getSource(user), (Product)getModelService().getSource(product));
	return (CustomerReviewModel)getModelService().get(review);
  }
public Integer getNumberOfReviewsInRange(ProductModel product, Double min, Double max)
  {
	return CustomerReviewExtendedManager.getInstance().getNumberOfReviewsInRange((Product)getModelService().getSource(product),min,max);
  }
}
